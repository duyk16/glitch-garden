﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefenderButton : MonoBehaviour {

    [SerializeField] Color inActiveColor = Color.gray;
    [SerializeField] Color activeColor = Color.white;
    [SerializeField] Defender defenderPrefab = null;

    void Start() {
        SetColor(inActiveColor);
    }

    private void OnMouseDown() {
        DefenderButton[] buttons = FindObjectsOfType<DefenderButton>();

        foreach (var button in buttons) {
            button.SetColor(inActiveColor);
        }
        SetColor(activeColor);
        FindObjectOfType<DefenderSpawner>().SetSelectedDefender(defenderPrefab);
    }

    public void SetColor(Color color) {
        GetComponent<SpriteRenderer>().color = color;
    }
}