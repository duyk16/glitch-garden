﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class Attacker : MonoBehaviour {
    float walkSpeed = 1f;

    public void SetMovementSpeed(float speed) {
        walkSpeed = speed;
    }

    void Update () {
        transform.Translate(Vector2.left * Time.deltaTime * walkSpeed);
    }
}
