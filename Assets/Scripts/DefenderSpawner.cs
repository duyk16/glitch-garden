﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefenderSpawner : MonoBehaviour {
    
    Defender defender = null;

    private void OnMouseDown() {
        SpawnDefender(GetSquareClicked());
    }

    public void SetSelectedDefender(Defender defenderToSelect) {
        defender = defenderToSelect;
    }

    Vector2 GetSquareClicked() {
        Vector2 clickPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        Vector2 worldPos = Camera.main.ScreenToWorldPoint(clickPos);
        Vector2 gribPos = SnapToGrid(worldPos);
        return gribPos;
    }

    Vector2 SnapToGrid(Vector2 rawPos) {
        float newX = Mathf.Round(rawPos.x);
        float newY = Mathf.Round(rawPos.y);
        return new Vector2(newX, newY);
    }

    void SpawnDefender(Vector2 position) {
        Defender newDefender = Instantiate(defender, position, Quaternion.identity);
    }
}