﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class AttackerSpawner : MonoBehaviour {
    [SerializeField] float minSpawnDelay = 1f;
    [SerializeField] float maxSpawnDelay = 5f;
    [SerializeField] Attacker attackerPrefab = null;

    bool spawn = true;

    // Start is called before the first frame update
    void Start() {
        StartCoroutine(SpawnerLoop());
    }

    IEnumerator SpawnerLoop() {
        while (spawn) {
            yield return new WaitForSeconds(Random.Range(minSpawnDelay, maxSpawnDelay));
            SpawnAttacker();
        }
    }

    void SpawnAttacker() {
        Instantiate(attackerPrefab, transform.position, Quaternion.identity);
    }

    // Update is called once per frame
    void Update() {

    }
}
