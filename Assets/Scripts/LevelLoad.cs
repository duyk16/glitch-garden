﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoad : MonoBehaviour {
    int currentSceneIndex;
    // Start is called before the first frame update
    void Start () {
        currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        if (currentSceneIndex == 0) {
            StartCoroutine(WaitForTime());
        }
    }
    
    IEnumerator WaitForTime() {
        yield return new WaitForSeconds(3);
        SceneManager.LoadScene("Start Scene");
    }



    // Update is called once per frame
    void Update () {

    }
}
